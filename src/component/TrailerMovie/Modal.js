import React from "react";
import "./Modal.css";
function Modal(props) {
  console.log("23", props.tempData);
  return (
    <div
      className="modal bg-black flex justify-center items-center"
      style={{ width: "75vw", height: "80vh" }}
      onClick={(e) => {
        e.stopPropagation();
      }}
    >
      <iframe
        width="100%"
        height="100%"
        src={props.tempData}
        title="YouTube video player"
        frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen
      ></iframe>
      <button
        className="btn-close"
        onClick={() => {
          props.setModal(false);
        }}
      >
        X
      </button>
    </div>
  );
}

export default Modal;

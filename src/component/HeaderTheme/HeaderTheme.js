import React from "react";
import UserNav from "./UserNav";
import { NavLink } from "react-router-dom";
import "./HeaderTheme.css";

let changeHref = (e) =>
  document.getElementById(e.target.name)?.scrollIntoView();

export default function HeaderTheme() {
  return (
    <div className="header container">
      <div className="header_nav px-10 py-1 mx-auto">
        <div style={{ color: "white" }} className="logo text-2xl font-medium">
          <img
            width="200"
            height="50"
            src="https://cybersoft.edu.vn/wp-content/uploads/2021/03/logo-cyber-nav.svg"
            alt=""
          ></img>
        </div>
        <div className="flex">
          <ul className="flex text-white items-center">
            <li className="flex">
              <NavLink
                to="/"
                className="flex text-xl px-4  dark:text-violet-400 dark:border-violet-400"
                name="lichChieu"
              >
                {
                  (onclick = (e) => {
                    changeHref(e);
                  })
                }
                Lịch chiếu
              </NavLink>
            </li>
            <li className="flex">
              <NavLink
                to="/"
                className="flex items-center text-xl px-4 "
                name="tabMovie"
              >
                {
                  (onclick = (e) => {
                    changeHref(e);
                  })
                }
                Cụm rạp
              </NavLink>
            </li>
            <li className="flex">
              <NavLink
                to="/"
                className="flex items-center text-xl px-4"
                name="news"
              >
                {
                  (onclick = (e) => {
                    changeHref(e);
                  })
                }
                Tin tức
              </NavLink>
            </li>
            <li className="flex">
              <NavLink
                to="/"
                className="flex items-center text-xl px-4"
                name="Application"
              >
                {
                  (onclick = (e) => {
                    changeHref(e);
                  })
                }
                Ứng dụng
              </NavLink>
            </li>
          </ul>
        </div>
        <div>
          <UserNav />
        </div>
      </div>
    </div>
  );
}

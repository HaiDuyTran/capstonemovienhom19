import axios from "axios";
import { https, TOKEN_CYBER } from "./configURL";
import { localStorageServ } from "./localStorageService";

export let QuanLyRapService = {
  layThongTinHeThongRap: () => {
    return https.get(`/api/QuanLyRap/LayThongTinHeThongRap?maHeThongRap`);
  },

  layDanhSachHeThongRap: () => {
    return https.get(
      `/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP05`
    );
  },

  layThongTinCumRapTheoHeThong: (heThongRap) => {
    return https.get(
      `/api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=${heThongRap}`
    );
  },

  taoLichChieu: (value) => {
    return axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyDatVe/TaoLichChieu",
      method: "POST",
      data: value,
      headers: {
        ["Authorization"]: "bearer " + localStorageServ.user?.get().accessToken,
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },
};

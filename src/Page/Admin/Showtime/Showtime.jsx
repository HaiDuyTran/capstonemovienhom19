import React, { useEffect, useState } from "react";
import { DatePicker, Space, InputNumber, Select, message, Form } from "antd";
import FormItem from "antd/es/form/FormItem";
import { useParams } from "react-router-dom";
import { QuanLyRapService } from "../../../services/QuanLyRapService";
import { useFormik } from "formik";
import moment from "moment";

export default function Showtime() {
  const { id } = useParams();
  const [state, setState] = useState({
    heThongRap: [],
    cumRap: [],
  });

  const formik = useFormik({
    initialValues: {
      maphim: id,
      ngayChieuGioChieu: "",
      maRap: "",
      giaVe: "",
    },
    onSubmit: async (values) => {
      try {
        let res = await QuanLyRapService.taoLichChieu(values);
        message.success(res.data.content);

        setTimeout(() => {
          window.location.href = "/admin/films";
        }, 1000);
      } catch (err) {
        message.warning(err.response.data.content);
      }
    },
  });

  useEffect(async () => {
    try {
      let res = await QuanLyRapService.layDanhSachHeThongRap();
      setState({
        ...state,
        heThongRap: res.data.content,
      });
    } catch (err) {}
  }, []);

  const handleChangeHTR = async (value) => {
    try {
      let res = await QuanLyRapService.layThongTinCumRapTheoHeThong(value);
      setState({
        ...state,
        cumRap: res.data.content,
      });
    } catch (err) {
      message.warning(err.response.data.content);
    }
  };

  const handleChangeCR = (value) => {
    formik.setFieldValue("maRap", value);
  };

  const onChangeDate = (value, dateString) => {
    formik.setFieldValue(
      "ngayChieuGioChieu",
      moment(value).format("DD/MM/YYYY hh:mm:ss")
    );
  };

  const onChangeNumber = (value) => {
    formik.setFieldValue("giaVe", value);
  };

  const onOk = (value) => {
    formik.setFieldValue(
      "ngayChieuGioChieu",
      moment(value).format("DD/MM/YYYY hh:mm:ss")
    );
  };

  const optionsSelectHeThongRap = () => {
    return state.heThongRap?.map((HTR, index) => {
      return {
        label: HTR.tenHeThongRap,
        value: HTR.tenHeThongRap,
      };
    });
  };

  const optionsSelectCumRap = () => {
    return state.cumRap?.map((cumRap, index) => {
      return {
        label: cumRap.tenCumRap,
        value: cumRap.maCumRap,
      };
    });
  };

  return (
    <div>
      <h3 className="text-4xl mb-5">Tạo lịch chiếu</h3>

      <Form
        name="basic"
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 12 }}
        initialValues={{ remember: true }}
        onSubmitCapture={formik.handleSubmit}
      >
        {" "}
        <FormItem label="Hệ thống rạp">
          <Select
            options={optionsSelectHeThongRap()}
            onChange={handleChangeHTR}
            placeholder="Chọn hệ thống rạp"
          />
        </FormItem>
        <FormItem label="Cụm rạp">
          <Select
            onChange={handleChangeCR}
            options={optionsSelectCumRap()}
            placeholder="Chọn cụm rạp"
          />
        </FormItem>
        <FormItem label="Ngày chiếu giờ chiếu">
          <Space direction="vertical" size={12}>
            <DatePicker
              format="DD/MM/YYYY   hh:mm:ss"
              style={{ width: "500px" }}
              showTime
              onChange={onChangeDate}
              onOk={onOk}
              placeholder="Chọn Ngày giờ chiếu"
            />
          </Space>
        </FormItem>
        <FormItem label="Giá vé">
          <Space>
            <InputNumber
              size="large"
              addonAfter="VNĐ"
              min={50000}
              max={150000}
              onChange={onChangeNumber}
            />
          </Space>
        </FormItem>
        <FormItem label="Tác vụ">
          <button
            type="submit"
            className="bg-blue-500 text-white font-semibold px-5 py-3 rounded-xl hover:bg-blue-300 hover:text-black"
          >
            Tạo lịch chiếu
          </button>
        </FormItem>
      </Form>
    </div>
  );
}

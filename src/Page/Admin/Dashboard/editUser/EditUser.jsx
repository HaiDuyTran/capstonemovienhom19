import React, { useState, useEffect } from "react";
import { Form, Input, Radio, Select } from "antd";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  capNhatThongTinNguoiDungAction,
  layThongTinNguoiDungCapNhatAction,
} from "../../../../redux/actions/userAction";
import { useFormik } from "formik";

export default function EditUser() {
  const dispatch = useDispatch();
  const [componentSize, setComponentSize] = useState("default");
  const [selected, setSelected] = useState();
  const { id } = useParams();
  const { thongTinNguoiDungCapNhat } = useSelector(
    (state) => state.userReducer
  );

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      taiKhoan: thongTinNguoiDungCapNhat.taiKhoan,
      hoTen: thongTinNguoiDungCapNhat.hoTen,
      matKhau: thongTinNguoiDungCapNhat.matKhau,
      email: thongTinNguoiDungCapNhat.email,
      maLoaiNguoiDung: thongTinNguoiDungCapNhat.maLoaiNguoiDung,
      maNhom: thongTinNguoiDungCapNhat.maNhom,
      soDT: thongTinNguoiDungCapNhat.soDT,
    },
    onSubmit: (values) => {
      dispatch(capNhatThongTinNguoiDungAction(values));
    },
  });

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleChangeSelect = (name) => {
    return (value) => {
      formik.setFieldValue(name, value);
    };
  };

  useEffect(() => {
    dispatch(layThongTinNguoiDungCapNhatAction(id));
  }, []);

  return (
    <div className="container">
      <h3 className="text-4xl mb-5">Cập nhật người dùng</h3>

      <Form
        onSubmitCapture={formik.handleSubmit}
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        initialValues={{
          size: componentSize,
        }}
        onValuesChange={onFormLayoutChange}
        size={componentSize}
      >
        <Form.Item label="Form Size" name="size">
          <Radio.Group>
            <Radio.Button value="small">Small</Radio.Button>
            <Radio.Button value="default">Default</Radio.Button>
            <Radio.Button value="large">Large</Radio.Button>
          </Radio.Group>
        </Form.Item>

        <Form.Item label="Tên người dùng" onChange={formik.handleChange}>
          <Input value={formik.values.hoTen} name="hoTen" />
        </Form.Item>

        <Form.Item label="Mật khẩu" onChange={formik.handleChange}>
          <Input value={formik.values.matKhau} name="matKhau" />
        </Form.Item>

        <Form.Item label="E-mail" onChange={formik.handleChange}>
          <Input value={formik.values.email} name="email" />
        </Form.Item>

        <Form.Item label="Loại người dùng">
          <Select
            defaultValue="1"
            onChange={handleChangeSelect("maLoaiNguoiDung")}
          >
            <Select.Option value="1">
              {formik.values.maLoaiNguoiDung}
            </Select.Option>
            <Select.Option value="QuanTri">QuanTri</Select.Option>
            <Select.Option value="KhachHang">KhachHang</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item label="Mã nhóm" onChange={formik.handleChange}>
          <Input value={formik.values.maNhom} name="maNhom" />
        </Form.Item>

        <Form.Item label="Số điện thoại" onChange={formik.handleChange}>
          <Input value={formik.values.soDT} name="soDT" />
        </Form.Item>

        <Form.Item label="Tác vụ">
          <button
            type="submit"
            className="p-2 bg-blue-400 rounded-lg text-white font-semibold hover:text-blue-800 hover:bg-white hover:border-2 hover:border-blue-800 align-middle"
          >
            CẬP NHẬT NGƯỜI DÙNG
          </button>
        </Form.Item>
      </Form>
    </div>
  );
}

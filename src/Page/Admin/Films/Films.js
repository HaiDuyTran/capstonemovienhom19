import React, { Fragment, useEffect } from "react";
import { Table } from "antd";
import { Input } from "antd";
import {
  SearchOutlined,
  EditOutlined,
  DeleteOutlined,
  CalendarOutlined,
} from "@ant-design/icons";
import "./Films.css";
import { useDispatch, useSelector } from "react-redux";
import {
  getMovieListActionService,
  xoaPhimAction,
} from "../../../redux/actions/movieAction";
import { NavLink } from "react-router-dom";

export default function Films() {
  // lấy movieList
  const { movieList } = useSelector((state) => state.movieReducer);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getMovieListActionService());
  }, []);

  // thanh search
  const { Search } = Input;

  const onSearch = (value) => {
    dispatch(getMovieListActionService(value));
  };

  // table
  const columns = [
    {
      title: "Mã Phim",
      dataIndex: "maPhim",
      width: "10%",
      //sort
      value: (text, object) => {
        return <span>{text}</span>;
      },
      sorter: (a, b) => b.maPhim - a.maPhim,
      sortDirections: ["descend", "ascend"],
    },

    {
      title: "Hình Ảnh",
      dataIndex: "hinhAnh",
      width: "20%",
      render: (text, film, index) => {
        return (
          <Fragment>
            <img
              src={film.hinhAnh}
              alt={film.tenPhim}
              width={50}
              height={50}
              onError={(e) => {
                e.target.onError = null;
                e.target.src = `https://picsum.photos/id/${index}/50/50`;
              }}
            />
          </Fragment>
        );
      },
      //   sorter: (a, b) => a.age - b.age,
    },

    {
      title: "Tên Phim",
      dataIndex: "tenPhim",
      width: "20%",
      sorter: (a, b) => {
        let tenPhimA = a.tenPhim.toLowerCase().trim();
        let tenPhimB = b.tenPhim.toLowerCase().trim();
        if (tenPhimA > tenPhimB) {
          return 1;
        }
        return -1;
      },
      sortDirections: ["descend", "ascend"],
    },

    {
      title: "Mô tả",
      dataIndex: "moTa",
      width: "40%",
      sorter: (a, b) => {
        let moTaA = a.moTa.toLowerCase().trim();
        let moTaB = b.moTa.toLowerCase().trim();
        if (moTaA > moTaB) {
          return 1;
        }
        return -1;
      },
      sortDirections: ["descend", "ascend"],
      render: (text, film, index) => {
        return (
          <Fragment>
            {film.moTa > 50 ? film.moTa.subStr(0, 50) + "..." : film.moTa}
          </Fragment>
        );
      },
    },

    {
      title: "Hành động",
      dataIndex: "hanhDong",
      width: "10%%",

      sortDirections: ["descend", "ascend"],
      render: (text, film, index) => {
        return (
          <Fragment>
            <NavLink
              key={1}
              className="mr-1 text-2xl"
              to={`/admin/films/edit/${film.maPhim}`}
            >
              <EditOutlined style={{ color: "blue" }} />
            </NavLink>

            <NavLink
              key={2}
              className="text-2xl"
              onClick={() => {
                deleteFilm(film);
              }}
            >
              <DeleteOutlined style={{ color: "red" }} />
            </NavLink>

            <NavLink
              key={1}
              className="ml-1 mr-1 text-2xl"
              to={`/admin/showtime/${film.maPhim}`}
            >
              <CalendarOutlined style={{ color: "green" }} />
            </NavLink>
          </Fragment>
        );
      },
    },
  ];

  const data = movieList;

  const deleteFilm = (film) => {
    if (window.confirm(`Bạn có muốn xoá phim: ${film.tenPhim} không??`)) {
      dispatch(xoaPhimAction(film.maPhim));
    }
  };

  const onChange = (pagination, filters, sorter, extra) => {
    console.log("params", pagination, filters, sorter, extra);
  };

  return (
    <div className="container">
      <h3 className="text-4xl">Quản lý phim</h3>
      {/* <button className="mt-5" onClick={() => {}}>
        Thêm phim
      </button> */}
      <Search
        className="my-5"
        placeholder="input search text"
        enterButton={<SearchOutlined />}
        size="large"
        onSearch={onSearch}
      />
      <Table columns={columns} dataSource={data} onChange={onChange} />
    </div>
  );
}

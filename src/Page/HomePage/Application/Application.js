import React from "react";
import "./Application.css";
export default function Application() {
  return (
    <div id="Application" className="mt-24">
      <div className="jss117">
        <div className="jss118">
          <div className="grid grid-cols-12 ">
            <div className="lg:col-span-6 ">
              <div className="jss119 text-center text-lg-left">
                <div>
                  <p className="jss120">Ứng dụng tiện lợi dành cho</p>
                  <p className="jss120">người yêu điện ảnh</p>
                  <br />
                  <p>
                    Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm
                    rạp và đổi quà hấp dẫn.
                  </p>
                  <br />
                  <button className="bg-red-500 rounded py-2 px-4">
                    Cài đặt Progressive App!
                  </button>
                  <br />
                  <p className="py-3 text-xl">
                    Tix có hai phiên bản{" "}
                    <span>
                      <a
                        href="https://itunes.apple.com/us/app/123phim-mua-ve-lien-tay-chon/id615186197?mt=8"
                        target="_blank"
                        rel="noopener noreferrer"
                        style={{ textDecoration: "underline" }}
                      >
                        IOS
                      </a>
                    </span>{" "}
                    và{" "}
                    <span>
                      <a
                        href="https://play.google.com/store/apps/details?id=vn.com.vng.phim123"
                        target="_blank"
                        rel="noopener noreferrer"
                        style={{ textDecoration: "underline" }}
                      >
                        Android
                      </a>
                    </span>
                  </p>
                </div>
              </div>
            </div>
            <div className="lg:col-span-6">
              <div className="jss121">
                <img
                  className="jss122"
                  src="https://freepngimg.com/thumb/smartphone/21604-1-smartphone-transparent-background.png"
                  alt="mobile"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

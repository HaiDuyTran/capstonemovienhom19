import React from "react";
import Application from "./Application/Application";
import HeaderPage from "./Header/HeaderPage";
import ListMovie from "./ListMovie/MutipleRowSlick";
import News from "./News/News";
import TabMovie from "./TabMovie/TabMovie";
import { ArrowUpOutlined } from "@ant-design/icons";

export default function HomePage() {
  return (
    <div>
      <HeaderPage />
      <ListMovie />
      <TabMovie />
      <News />
      <Application />
      {/* go up button */}
      <div>
        <a
          href="#header"
          className="fixed bottom-4 right-4 text-orange-500 hover:text-blue-500"
        >
          <ArrowUpOutlined style={{ fontSize: "50px" }} />
        </a>
      </div>
    </div>
  );
}

import { Tabs } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
  batLoadingAction,
  tatLoadingAction,
} from "../../../redux/actions/spinnerAction";
import { movieService } from "../../../services/movie.service";
import ItemTabMovie from "./ItemTabMovie";

const { TabPane } = Tabs;

export default function TabMovie() {
  const [dataMovie, setDataMovie] = useState([]);

  let dispatch = useDispatch();

  useEffect(() => {
    dispatch(batLoadingAction());
    movieService
      .getMovieByTheater()
      .then((res) => {
        setDataMovie(res.data.content);
        dispatch(tatLoadingAction());
      })
      .catch((err) => {
        dispatch(tatLoadingAction());
      });
  }, []);

  const onChange = (key) => {
    console.log(key);
  };

  const renderContent = () => {
    return dataMovie.map((heThongRap, index) => {
      return (
        <TabPane
          tab={<img className="w-16" src={heThongRap.logo} />}
          key={index}
        >
          <Tabs
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            style={{ height: 500 }}
          >
            {heThongRap.lstCumRap.map((cumRap) => {
              return (
                <TabPane tab={renderTenCumRap(cumRap)} key={cumRap.maCumRap}>
                  <div
                    style={{ height: 500, overflowY: "scroll" }}
                    className="shadow-xl grid gap-3"
                  >
                    {cumRap.danhSachPhim.map((phim) => {
                      return <ItemTabMovie phim={phim} />;
                    })}
                  </div>
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };

  const renderTenCumRap = (cumRap) => {
    return (
      <div className="text-left w-60 whitespace-normal">
        <p className="text-green-700">{cumRap.tenCumRap}</p>
        <p className="text-black truncate">{cumRap.diaChi}</p>
        <button className="text-red-600">{"{ Xem chi tiết }"}</button>
      </div>
    );
  };

  return (
    <div className="py-20 mt-10 px-20" id="tabMovie">
      <Tabs
        style={{ height: 500 }}
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
      >
        {renderContent()}
      </Tabs>
    </div>
  );
}

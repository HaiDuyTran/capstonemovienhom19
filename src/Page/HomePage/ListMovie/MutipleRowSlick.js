import React, { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Slider from "react-slick";
import { getMovieListActionService } from "../../../redux/actions/movieAction";
import {
  SET_DANG_CHIEU,
  SET_SAP_CHIEU,
} from "../../../redux/constant/movieConstant";
import "./MutipleRowSlick.css";
import Modal from "../../../component/TrailerMovie/Modal";

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div>
      <div
        className={className}
        style={{ ...style, display: "block" }}
        onClick={onClick}
      />
    </div>
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block" }}
      onClick={onClick}
    />
  );
}

export default function ListMovie() {
  //state.popup
  const [modal, setModal] = useState(false);
  const [tempData, setTempData] = useState([]);
  let dispatch = useDispatch();
  let { movieList } = useSelector((state) => {
    return state.movieReducer;
  });
  // state.movieList
  const { dangChieu, sapChieu } = useSelector((state) => state.movieReducer);

  useEffect(() => {
    dispatch(getMovieListActionService());
  }, []);

  const settings = {
    className: "center variable-width",
    centerMode: true,
    infinite: true,
    centerPadding: "0px",
    variableWidth: true,
    slidesToShow: 4,
    speed: 500,
    rows: 2,
    slidesPerRow: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  //Pop up trailer
  const getData = (trailer) => {
    let tempData = [trailer];

    setTempData((item) => [1, ...tempData]);
    return setModal(true);
  };

  //renderMovieList
  let renderMovieList = () => {
    return movieList
      .filter((item) => {
        return item.hinhAnh !== null;
      })
      .map((item, index) => {
        item.trailer = item.trailer.replace("watch?v=", "embed/");

        return (
          <div key={index}>
            <div className="swiper-slide">
              <div className="slide-content">
                <img
                  src={item.hinhAnh}
                  alt="Avatar"
                  style={{
                    width: 300,
                    height: 300,
                    backgroundSize: "100%",
                  }}
                />
                <div className="content-overlay">
                  <div
                    className="w-full h-full flex justify-center items-center"
                    style={{
                      position: "absolute",
                      backgroundColor: "rgba(0,0,0,.5)",
                    }}
                  >
                    {/* trailer */}
                    <div>
                      <img
                        src="	https://movie-booking-project.vercel.app/img/carousel/play-video.png"
                        style={{
                          position: "absolute",
                          top: "18%",
                          left: "37%",
                          cursor: "pointer",
                        }}
                        alt="play"
                        onClick={() => {
                          // handleButtonPopup(item.maPhim);
                          getData(item.trailer);
                        }}
                      ></img>
                    </div>
                    <div className="text-2xl mt-2 font-bold">{item.biDanh}</div>
                  </div>
                </div>
              </div>
              <div className="mt-2">
                <button
                  onClick={() => {
                    window.location.href = `/detail/${item.maPhim}`;
                  }}
                  className="bg-blue-400 rounded text-center cursor-pointer py-2 my-2 text-success-50 font-bold w-11/12 mr-2"
                >
                  Đặt vé
                </button>
              </div>
            </div>
          </div>
        );
      });
  };

  let activeClassDC = dangChieu === true ? "active_film" : "non_active_film";
  let activeClassSC = sapChieu === true ? "active_film" : "non_active_film";

  return (
    <div className="px-20 mt-10 movieList" id="lichChieu">
      {modal === true ? (
        <div
          className="modal-container"
          onClick={() => {
            setModal(false);
          }}
        >
          {" "}
          <Modal tempData={tempData[1]} setModal={setModal} />
        </div>
      ) : (
        ""
      )}
      <button
        type="button"
        className={`${activeClassDC} px-8 py-3 text-2xl font-semibold border rounded mr-10`}
        onClick={() => {
          const action = { type: SET_DANG_CHIEU };
          dispatch(action);
        }}
      >
        Đang chiếu
      </button>
      <button
        type="button"
        className={`${activeClassSC} px-8 py-3 text-2xl font-semibold border rounded mb-3`}
        onClick={() => {
          const action = { type: SET_SAP_CHIEU };
          dispatch(action);
        }}
      >
        Sắp chiếu
      </button>
      <Slider {...settings}>{renderMovieList()}</Slider>
    </div>
  );
}

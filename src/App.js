import "./App.css";
import LoginPage from "./Page/LoginPage/LoginPage";
import HomePage from "./Page/HomePage/HomePage";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Layout from "./HOC/Layout";
import DetailPage from "./Page/DetailPage/DetailPage";
import BookingPage from "./Page/BookingPage/BookingPage.jsx";
import SpinnerComponent from "./component/SpinnerComponent/SpinnerComponent";
import RegisterPage from "./Page/RegisterPage/RegisterPage";
import BookingHistory from "./Page/BookingHistory/BookingHistory";
import AdminLayout from "./HOC/AdminLayout/AdminLayout";
import DashBoard from "./Page/Admin/Dashboard/DashBoard.jsx";
import Films from "./Page/Admin/Films/Films";
import Showtime from "./Page/Admin/Showtime/Showtime";
import AddNew from "./Page/Admin/Films/AddNew/AddNew";
import Edit from "./Page/Admin/Films/Edit/Edit";
import EditUser from "./Page/Admin/Dashboard/editUser/EditUser";
import addUser from "./Page/Admin/Dashboard/addUser/addUser";

function App() {
  return (
    <div className="App">
      <SpinnerComponent />
      <BrowserRouter>
        <Routes>
          {/* Homepage template */}
          <Route path="/" element={<Layout Component={HomePage} />} />

          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          />
          <Route path="/booking/:id" element={<BookingPage />} />

          <Route path="/history" element={<BookingHistory />} />
          {/* user template */}
          <Route path="/login" element={<LoginPage />} />

          <Route path="/register" element={<RegisterPage />} />
          {/* admin template */}

          <Route
            path="/admin/users"
            element={<AdminLayout Component={DashBoard} />}
          />
          <Route
            path="/admin/users/edit/:id"
            element={<AdminLayout Component={EditUser} />}
          />
          <Route
            path="/admin/users/addUser"
            element={<AdminLayout Component={addUser} />}
          />
          <Route
            path="/admin/films"
            element={<AdminLayout Component={Films} />}
          />
          <Route
            path="/admin/films/addnew"
            element={<AdminLayout Component={AddNew} />}
          />
          <Route
            path="/admin/showtime/:id"
            element={<AdminLayout Component={Showtime} />}
          />
          <Route
            path="/admin/films/edit/:id"
            element={<AdminLayout Component={Edit} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;

import { message } from "antd";
import { movieService } from "../../services/movie.service";
import { GET_THONG_TIN_PHIM, SET_MOVIE_LIST } from "../constant/movieConstant";

export const getMovieListActionService = (tenPhim = "") => {
  return (dispatch) => {
    movieService
      .getMovieList(tenPhim)
      .then((res) => {
        dispatch({
          type: SET_MOVIE_LIST,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const themPhimUploadHinhAction = (formData) => {
  return async (dispatch) => {
    try {
      let result = await movieService.themPhimUpLoadHinh(formData);
      message.success("Thêm phim thành công");
      console.log("result", result.data.content);
    } catch (errors) {
      console.log(errors.response?.data);
    }
  };
};

export const layThongTinPhimAction = (maPhim) => {
  return (dispatch) => {
    movieService
      .layThongTinPhim(maPhim)
      .then((res) => {
        dispatch({
          type: GET_THONG_TIN_PHIM,
          payload: res.data.content,
        });
      })
      .catch((err) => {});
  };
};

export const capNhatPhimUpLoadAction = (formData) => {
  return (dispatch) => {
    movieService
      .capNhatPhimUpLoad(formData)
      .then((res) => {
        message.success("CẬP NHẬT PHIM THÀNH CÔNG!!!");
        dispatch(getMovieListActionService());
      })
      .catch((err) => {
        message.warning(err.response.data.content);
      });
  };
};

export const xoaPhimAction = (maPhim) => {
  return (dispatch) => {
    movieService
      .xoaPhim(maPhim)
      .then((res) => {
        message.success("Xoá phim thành công");
        dispatch(getMovieListActionService());
      })
      .catch((err) => {});
  };
};

import { localStorageServ } from "../../services/localStorageService";
import {
  GET_DANH_SACH_NGUOI_DUNG,
  GET_TIM_KIEM_NGUOI_DUNG,
  POST_LAY_THONG_TIN_NGUOI_DUNG,
  SET_THONG_TIN_NGUOI_DUNG,
} from "../constant/useConstant";

let initialState = {
  userInfor: localStorageServ.user.get(),
  thongTinNguoiDung: {},
  danhSachNguoiDung: [],
  thongTinNguoiDungCapNhat: {},
};

export let userReducer = (state = initialState, action) => {
  switch (action.type) {
    case "LOGIN": {
      state.userInfor = action.payload;
      return { ...state };
    }

    case SET_THONG_TIN_NGUOI_DUNG: {
      state.thongTinNguoiDung = action.payload;
      return { ...state };
    }

    case GET_DANH_SACH_NGUOI_DUNG: {
      state.danhSachNguoiDung = action.payload;
      return { ...state };
    }

    case POST_LAY_THONG_TIN_NGUOI_DUNG: {
      state.thongTinNguoiDungCapNhat = action.payload;
      return { ...state };
    }

    case GET_TIM_KIEM_NGUOI_DUNG: {
      state.danhSachNguoiDung = action.payload;
    }

    default:
      return state;
  }
};

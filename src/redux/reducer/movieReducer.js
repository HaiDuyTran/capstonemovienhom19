import { Action } from "@remix-run/router";
import {
  CAP_NHAT_PHIM_UPLOAD,
  GET_THONG_TIN_PHIM,
  SET_DANG_CHIEU,
  SET_MOVIE_LIST,
  SET_SAP_CHIEU,
} from "../constant/movieConstant";

const initialState = {
  movieList: [],
  movieListDefault: [],
  dangChieu: true,
  sapChieu: true,
  thongTinPhim: {},
};

export const movieReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_MOVIE_LIST:
      state.movieList = payload;
      state.movieListDefault = state.movieList;
      return { ...state };

    case SET_DANG_CHIEU:
      state.dangChieu = !state.dangChieu;
      state.movieList = state.movieListDefault.filter(
        (film) => film.dangChieu === state.dangChieu
      );
      return { ...state };

    case SET_SAP_CHIEU:
      state.sapChieu = !state.sapChieu;
      state.movieList = state.movieListDefault.filter(
        (film) => film.sapChieu === state.sapChieu
      );
      return { ...state };

    case GET_THONG_TIN_PHIM: {
      state.thongTinPhim = payload;
      return { ...state };
    }

    case CAP_NHAT_PHIM_UPLOAD: {
    }
    default:
      return state;
  }
};
// rxreducer
